"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import redirect, url_for, request, render_template
from pymongo import MongoClient
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)

# create database called appdb
db = client.appdb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    brevet_dist = int(request.args.get('brevet_dist'))
    date = request.args.get('date')
    time = request.args.get('time')
    datetime = arrow.get(f"{date} {time}", 'YYYY-MM-DD HH:mm')
    app.logger.debug("km={}".format(km))
    app.logger.debug(f"brevet_distance={brevet_dist}")
    app.logger.debug("request.args: {}".format(request.args))

    open_time = acp_times.open_time(km, brevet_dist, datetime)
    close_time = acp_times.close_time(km, brevet_dist, datetime)
    result = {"open": open_time.isoformat(), "close": close_time.isoformat()}
    return flask.jsonify(result=result)

@app.route("/submit", methods=['POST'])
def submit():
    """ submit data from each user-inputted control into mongodb database """
    
    # clear existing data in db collection named 'brevet'. Works even if db is empty
    db.brevet.drop()

    miles = request.form.getlist('miles')
    kms = request.form.getlist('km')
    locations = request.form.getlist('location')
    open_times = request.form.getlist('open')
    close_times = request.form.getlist('close')

    for j in range(len(open_times)):
        # check if valid row in table by checking if open time is valid
        if open_times[j] != '':
            brevet_doc = {
                'begin_date': request.form['begin_date'],
                'begin_time': request.form['begin_time'],
                'miles': miles[j],
                'km': kms[j],
                'location': locations[j], 
                'open_time': open_times[j],
                'close_time': close_times[j],

            }
            db.brevet.insert_one(brevet_doc)

    return render_template('calc.html')


@app.route("/display", methods=['GET'])
def display():
    """ return new page displaying all brevet information """
    _items = db.brevet.find()
    items = [item for item in _items]
    if items == []:
        return render_template('empty.html')
    else:
        return render_template('display.html', items=items)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
